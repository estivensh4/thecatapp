package com.estiven.thecatapp.domain.use_case

import com.estiven.thecatapp.domain.model.ImagesInDTO
import com.estiven.thecatapp.domain.repository.TheCatRepository
import com.google.common.truth.Truth.assertThat
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class GetImageUseCaseTest {

    private lateinit var getImageUseCase: GetImageUseCase
    private lateinit var theCatRepository: TheCatRepository
    private lateinit var id: String

    @Before
    fun setUp() {
        id = "jesse+1=&"
        theCatRepository = mockk(relaxed = true)
        coEvery {
            theCatRepository.getImage(id).getOrThrow()
        } returns ImagesInDTO("https://s3.amazonaws.com/freecodecamp/relaxing-cat.jpg")
        getImageUseCase = GetImageUseCase(theCatRepository)
    }

    @Test
    fun `verify is call get image by id`() = runBlocking {
        val result = getImageUseCase(id).getOrThrow()
        val imagesInDTO = result.url
        assertThat(imagesInDTO).isEqualTo(result.url)
    }
}