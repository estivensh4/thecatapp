package com.estiven.thecatapp.data.repository

import com.estiven.thecatapp.data.remote.TheCatApi
import com.estiven.thecatapp.data.repository.remote.invalidResponseGetAllBreeds
import com.estiven.thecatapp.data.repository.remote.invalidResponseGetImage
import com.estiven.thecatapp.data.repository.remote.validResponseGetAllBreeds
import com.estiven.thecatapp.data.repository.remote.validResponseGetImage
import com.google.common.truth.Truth
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class TheCatRepositoryImplTest {
    private lateinit var theCatRepositoryImpl: TheCatRepositoryImpl
    private lateinit var mockWebServer: MockWebServer
    private lateinit var okHttpClient: OkHttpClient
    private lateinit var theCatApi: TheCatApi

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        okHttpClient = OkHttpClient.Builder()
            .writeTimeout(1, TimeUnit.SECONDS)
            .readTimeout(1, TimeUnit.SECONDS)
            .connectTimeout(1, TimeUnit.SECONDS)
            .build()
        theCatApi = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .baseUrl(mockWebServer.url("/"))
            .build()
            .create(TheCatApi::class.java)
        theCatRepositoryImpl = TheCatRepositoryImpl(
            theCatApi = theCatApi
        )
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }


    @Test
    fun `get all breeds, valid response, return success`() = runBlocking {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(200)
                .setBody(validResponseGetAllBreeds)
        )
        val result = theCatRepositoryImpl.getAllBreeds(
            page = 1,
            limit = 10
        )
        Truth.assertThat(result.isSuccess).isTrue()
    }

    @Test
    fun `get all breeds, invalid response, return failure`() = runBlocking {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(403)
                .setBody(invalidResponseGetAllBreeds)
        )
        val result = theCatRepositoryImpl.getAllBreeds(
            page = 1,
            limit = 10
        )
        Truth.assertThat(result.isFailure).isTrue()
    }

    @Test
    fun `get all breeds, malformed response, return failure`() = runBlocking {
        mockWebServer.enqueue(
            MockResponse().setBody(invalidResponseGetAllBreeds)
        )
        val result = theCatRepositoryImpl.getAllBreeds(
            page = 1,
            limit = 10
        )
        Truth.assertThat(result.isFailure).isTrue()
    }

    @Test
    fun `get image, valid response, return success`() = runBlocking {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(200)
                .setBody(validResponseGetImage)
        )
        val result = theCatRepositoryImpl.getImage("ixD92/2==")
        Truth.assertThat(result.isSuccess).isTrue()
    }

    @Test
    fun `get image, invalid response, return failure`() = runBlocking {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(403)
                .setBody(invalidResponseGetImage)
        )
        val result = theCatRepositoryImpl.getImage("ixD92/2==")
        Truth.assertThat(result.isFailure).isTrue()
    }

    @Test
    fun `get image, malformed response, return failure`() = runBlocking {
        mockWebServer.enqueue(
            MockResponse().setBody(invalidResponseGetImage)
        )
        val result = theCatRepositoryImpl.getImage("ixD92/2==")
        Truth.assertThat(result.isFailure).isTrue()
    }
}