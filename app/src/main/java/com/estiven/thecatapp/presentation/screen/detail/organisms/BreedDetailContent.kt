package com.estiven.thecatapp.presentation.screen.detail.organisms

import android.content.Context
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.estiven.thecatapp.domain.model.BreedsInDTO
import com.estiven.thecatapp.presentation.screen.detail.BreedDetailViewModel
import com.estiven.thecatapp.presentation.screen.detail.molecules.BreedDetailFooter
import com.estiven.thecatapp.shared.components.CustomImageNetwork

@Composable
fun BreedDetailContent(
    modifier: Modifier,
    breed: BreedsInDTO,
    breedDetailViewModel: BreedDetailViewModel,
    context: Context
) {
    Box(
        modifier = modifier.fillMaxSize(),
        contentAlignment = Alignment.BottomCenter
    ) {
        CustomImageNetwork(
            url = breed.image,
            contentDescription = breed.name,
            modifier = Modifier
                .align(Alignment.TopCenter)
                .fillMaxWidth()
                .height(420.dp)
        )
        BreedDetailFooter(
            breed = breed,
            breedDetailViewModel = breedDetailViewModel,
            context = context
        )
    }
}