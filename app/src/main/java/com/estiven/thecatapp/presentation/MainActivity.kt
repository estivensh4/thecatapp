package com.estiven.thecatapp.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.estiven.thecatapp.domain.model.BreedsInDTO
import com.estiven.thecatapp.presentation.screen.detail.pages.BreedDetailScreen
import com.estiven.thecatapp.presentation.screen.list.screen.BreedsListScreen
import com.estiven.thecatapp.presentation.theme.TheCatAppTheme
import com.estiven.thecatapp.presentation.util.Screen
import com.estiven.thecatapp.presentation.util.fromJson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TheCatAppTheme {
                // A surface container using the 'background' color from the theme
                val navController = rememberNavController()
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    NavHost(
                        navController = navController,
                        startDestination = Screen.BREEDS_LIST.route
                    ) {
                        composable(Screen.BREEDS_LIST.route) {
                            BreedsListScreen(navController = navController)
                        }
                        composable("${Screen.BREED_DETAIL.route}?breed={breed}") { navBackStackEntry ->
                            navBackStackEntry.arguments?.let { bundle ->
                                val breed =
                                    bundle.getString("breed")?.fromJson(BreedsInDTO::class.java)
                                        ?: BreedsInDTO()
                                BreedDetailScreen(navController = navController, breed = breed)
                            }
                        }
                    }
                }
            }
        }
    }
}