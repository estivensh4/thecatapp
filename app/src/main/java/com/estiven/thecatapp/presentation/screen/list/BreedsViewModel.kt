package com.estiven.thecatapp.presentation.screen.list

import android.content.res.Configuration
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LoadState
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.compose.LazyPagingItems
import com.estiven.thecatapp.data.source.BreedsPagingSource
import com.estiven.thecatapp.data.util.UiEvent
import com.estiven.thecatapp.domain.model.BreedsInDTO
import com.estiven.thecatapp.domain.repository.TheCatRepository
import com.estiven.thecatapp.domain.use_case.UseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BreedsViewModel @Inject constructor(
    private val useCases: UseCases,
    private val theCatRepository: TheCatRepository
) : ViewModel() {

    private var _uiEvent = Channel<UiEvent>()
    val uiEvent = _uiEvent.receiveAsFlow()
    var resultPager by mutableStateOf(
        Pager(
            config = PagingConfig(
                pageSize = 5,
                initialLoadSize = 5,
                prefetchDistance = 1
            ),
            pagingSourceFactory = {
                BreedsPagingSource(theCatRepository)
            }
        ).flow
    )

    fun setupImage(breed: BreedsInDTO) {
        viewModelScope.launch {
            useCases.getImageUseCase(breed.referenceImageId)
                .onSuccess { imagesInDTO ->
                    breed.image = imagesInDTO.url
                }
        }
    }

    fun validateLandScape(configuration: Configuration): Int {
        return when (configuration.orientation) {
            Configuration.ORIENTATION_LANDSCAPE -> 4
            else -> 2
        }
    }

    fun loadingAppended(breedList: LazyPagingItems<BreedsInDTO>): Boolean {
        return when (breedList.loadState.append) {
            is LoadState.NotLoading -> false
            is LoadState.Loading -> true
            else -> false
        }
    }

    fun loadingRefresh(breedList: LazyPagingItems<BreedsInDTO>): Boolean {
        return when (breedList.loadState.refresh) {
            is LoadState.NotLoading -> false
            is LoadState.Loading -> true
            else -> false
        }
    }
}