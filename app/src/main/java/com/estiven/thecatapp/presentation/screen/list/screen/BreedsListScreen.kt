package com.estiven.thecatapp.presentation.screen.list.screen

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.paging.compose.collectAsLazyPagingItems
import com.estiven.thecatapp.R
import com.estiven.thecatapp.data.util.UiEvent
import com.estiven.thecatapp.presentation.screen.list.BreedsViewModel
import com.estiven.thecatapp.presentation.screen.list.molecules.ItemBreed
import com.estiven.thecatapp.presentation.util.Screen.BREED_DETAIL
import com.estiven.thecatapp.presentation.util.toJson
import com.estiven.thecatapp.shared.components.CustomScaffold
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

@Composable
fun BreedsListScreen(
    navController: NavController,
    breedsViewModel: BreedsViewModel = hiltViewModel()
) {
    val breedsList = breedsViewModel.resultPager.collectAsLazyPagingItems()
    val snackbarHostState = remember { SnackbarHostState() }
    val localConfiguration = LocalConfiguration.current
    LaunchedEffect(key1 = true) {
        breedsViewModel.uiEvent.collect { event ->
            when (event) {
                is UiEvent.Navigate -> navController.navigate(event.screen.route)
                is UiEvent.ShowSnackBar -> {
                    snackbarHostState.showSnackbar(event.message)
                }
            }
        }
    }
    CustomScaffold(
        title = stringResource(id = R.string.app_name), snackbarHostState = snackbarHostState
    ) { paddingValues ->
        Column(
            modifier = Modifier.padding(paddingValues)
        ) {
            LazyVerticalGrid(
                columns = GridCells.Fixed(breedsViewModel.validateLandScape(localConfiguration)),
                horizontalArrangement = Arrangement.spacedBy(16.dp),
                verticalArrangement = Arrangement.spacedBy(16.dp),
                contentPadding = PaddingValues(16.dp),
                modifier = Modifier.testTag(stringResource(id = R.string.test_tag_breed_list))
            ) {
                items(breedsList.itemCount) { position ->
                    breedsList[position]?.let { breed ->
                        breedsViewModel.setupImage(breed)
                        ItemBreed(breed) {
                            val breedParam = breed.copy(
                                image = URLEncoder.encode(
                                    breed.image, StandardCharsets.UTF_8.toString()
                                ), wikipediaUrl = URLEncoder.encode(
                                    breed.wikipediaUrl, StandardCharsets.UTF_8.toString()
                                )
                            )
                            navController.navigate(
                                "${BREED_DETAIL.route}?breed=${breedParam.toJson()}"
                            )
                        }
                    }
                }
            }
        }
        AnimatedVisibility(visible = breedsViewModel.loadingRefresh(breedsList)) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(MaterialTheme.colorScheme.primary.copy(0.5f)),
                contentAlignment = Alignment.Center
            ) {
                CircularProgressIndicator()
            }
        }
        if (breedsViewModel.loadingAppended(breedsList)) {
            Box(
                modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.BottomCenter
            ) {
                CircularProgressIndicator()
            }
        }
    }
}