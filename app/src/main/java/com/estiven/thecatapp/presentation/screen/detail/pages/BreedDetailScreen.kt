package com.estiven.thecatapp.presentation.screen.detail.pages

import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.estiven.thecatapp.R
import com.estiven.thecatapp.domain.model.BreedsInDTO
import com.estiven.thecatapp.presentation.screen.detail.BreedDetailViewModel
import com.estiven.thecatapp.presentation.screen.detail.organisms.BreedDetailContent
import com.estiven.thecatapp.shared.components.CustomScaffold

@Composable
fun BreedDetailScreen(
    breed: BreedsInDTO,
    navController: NavController,
    breedDetailViewModel: BreedDetailViewModel = hiltViewModel()
) {
    val snackbarHostState = remember { SnackbarHostState() }
    val context = LocalContext.current
    CustomScaffold(
        title = breed.name,
        snackbarHostState = snackbarHostState,
        navigationIcon = {
            IconButton(onClick = { navController.popBackStack() }) {
                Icon(
                    imageVector = Icons.Rounded.ArrowBack,
                    contentDescription = stringResource(id = R.string.content_description_back_arrow),
                )
            }
        }
    ) { paddingValues ->
        BreedDetailContent(
            modifier = Modifier.padding(paddingValues),
            breed = breed,
            breedDetailViewModel = breedDetailViewModel,
            context = context
        )
    }
}