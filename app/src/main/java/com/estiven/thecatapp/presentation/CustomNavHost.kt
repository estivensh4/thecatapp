package com.estiven.thecatapp.presentation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.estiven.thecatapp.domain.model.BreedsInDTO
import com.estiven.thecatapp.presentation.screen.detail.pages.BreedDetailScreen
import com.estiven.thecatapp.presentation.screen.list.screen.BreedsListScreen
import com.estiven.thecatapp.presentation.util.Screen
import com.estiven.thecatapp.presentation.util.fromJson

@Composable
fun CustomNavHost(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = Screen.BREEDS_LIST.route
    ) {
        composable(Screen.BREEDS_LIST.route) {
            BreedsListScreen(navController = navController)
        }
        composable("${Screen.BREED_DETAIL.route}?breed={breed}") { navBackStackEntry ->
            navBackStackEntry.arguments?.let { bundle ->
                val breed =
                    bundle.getString("breed")?.fromJson(BreedsInDTO::class.java)
                        ?: BreedsInDTO()
                BreedDetailScreen(navController = navController, breed = breed)
            }
        }
    }
}
