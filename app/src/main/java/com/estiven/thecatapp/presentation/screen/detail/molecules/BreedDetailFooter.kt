package com.estiven.thecatapp.presentation.screen.detail.molecules

import android.content.Context
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.FitnessCenter
import androidx.compose.material.icons.rounded.LocationOn
import androidx.compose.material.icons.rounded.Pets
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.estiven.thecatapp.R
import com.estiven.thecatapp.domain.model.BreedsInDTO
import com.estiven.thecatapp.presentation.screen.detail.BreedDetailViewModel
import com.estiven.thecatapp.presentation.util.findActivity

@Composable
fun BreedDetailFooter(
    breed: BreedsInDTO,
    breedDetailViewModel: BreedDetailViewModel,
    context: Context
) {
    val temperamentList = breed.temperament.split(",")
    Card(
        shape = RoundedCornerShape(topStart = 24.dp, topEnd = 24.dp),
        colors = CardDefaults.cardColors(
            containerColor = Color.White
        ),
    ) {
        Column(
            modifier = Modifier
                .padding(horizontal = 26.dp, vertical = 16.dp)
                .fillMaxWidth(),
            verticalArrangement = Arrangement.Bottom
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = breed.name,
                    style = MaterialTheme.typography.headlineMedium.copy(
                        fontWeight = FontWeight.ExtraBold
                    )
                )
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(text = stringResource(id = R.string.text_weight, breed.weight.metric))
                    Icon(
                        imageVector = Icons.Rounded.FitnessCenter,
                        contentDescription = breed.weight.metric
                    )
                }
            }
            Spacer(modifier = Modifier.size(12.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(
                        imageVector = Icons.Rounded.LocationOn,
                        contentDescription = stringResource(
                            id = R.string.content_description_location
                        )
                    )
                    Text(text = breed.origin)
                }
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(text = stringResource(id = R.string.text_life_span, breed.lifeSpan))
                    Icon(
                        imageVector = Icons.Rounded.Pets,
                        contentDescription = breed.lifeSpan
                    )
                }
            }
            LazyRow(
                contentPadding = PaddingValues(vertical = 16.dp),
                horizontalArrangement = Arrangement.spacedBy(6.dp)
            ) {
                items(temperamentList.size) { position ->
                    ItemTemperament(temperament = temperamentList[position])
                }
            }
            Text(
                text = stringResource(id = R.string.text_summary),
                style = MaterialTheme.typography.labelLarge.copy(
                    fontWeight = FontWeight.Medium
                )
            )
            Text(
                text = breed.description,
                style = MaterialTheme.typography.bodySmall.copy(
                    fontWeight = FontWeight.Normal
                )
            )
            Spacer(modifier = Modifier.size(24.dp))
            Button(
                onClick = {
                    breedDetailViewModel.openExternalUrl(
                        url = breed.wikipediaUrl,
                        activity = context.findActivity()
                    )
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .defaultMinSize(
                        minHeight = 56.dp
                    ),
                shape = RoundedCornerShape(6.dp),
                colors = ButtonDefaults.buttonColors(
                    containerColor = MaterialTheme.colorScheme.secondary
                )
            ) {
                Text(
                    text = stringResource(id = R.string.text_button_more_info),
                    color = MaterialTheme.colorScheme.onSecondary
                )
            }
        }
    }
}