package com.estiven.thecatapp.presentation.util

enum class Screen(val route: String) {
    BREEDS_LIST("breeds_list"),
    BREED_DETAIL("breeds_detail")
}