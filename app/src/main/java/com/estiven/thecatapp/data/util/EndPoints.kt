package com.estiven.thecatapp.data.util

object EndPoints {
    const val BREEDS = "/v1/breeds"
    const val IMAGES = "/v1/images/{id}"
}