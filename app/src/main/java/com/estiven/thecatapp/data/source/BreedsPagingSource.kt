package com.estiven.thecatapp.data.source

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.estiven.thecatapp.domain.model.BreedsInDTO
import com.estiven.thecatapp.domain.repository.TheCatRepository

class BreedsPagingSource(
    private val theCatRepository: TheCatRepository,
) : PagingSource<Int, BreedsInDTO>() {

    override fun getRefreshKey(state: PagingState<Int, BreedsInDTO>): Int? {
        return state.anchorPosition?.let { position ->
            val page = state.closestPageToPosition(position)
            page?.prevKey?.minus(1) ?: page?.nextKey?.plus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, BreedsInDTO> {
        val page = params.key ?: 1
        return try {
            val response = theCatRepository.getAllBreeds(
                page = page,
                limit = params.loadSize,
            )
            val breedsList = response.getOrNull()
            if (breedsList != null) {
                LoadResult.Page(
                    data = breedsList,
                    prevKey = if (page == 1) null else page - 1,
                    nextKey = if (breedsList.isNotEmpty()) page + 1 else null
                )
            } else {
                LoadResult.Error(
                    response.exceptionOrNull() ?: Throwable(
                        response.exceptionOrNull()?.message
                    )
                )
            }
        } catch (exception: Exception) {
            LoadResult.Error(exception)
        }
    }
}