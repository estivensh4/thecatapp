package com.estiven.thecatapp.data.di

import com.estiven.thecatapp.domain.repository.TheCatRepository
import com.estiven.thecatapp.domain.use_case.GetImageUseCase
import com.estiven.thecatapp.domain.use_case.UseCases
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object AppDomainModule {

    @ViewModelScoped
    @Provides
    fun provideAppUseCases(
        theCatRepository: TheCatRepository
    ): UseCases {
        return UseCases(
            getImageUseCase = GetImageUseCase(theCatRepository)
        )
    }
}