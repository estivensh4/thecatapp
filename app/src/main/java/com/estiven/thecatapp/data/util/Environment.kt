package com.estiven.thecatapp.data.util

object Environment {
    const val BASE_URL = "https://api.thecatapi.com/"
}