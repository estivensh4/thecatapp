package com.estiven.thecatapp.data.repository

import com.estiven.thecatapp.data.remote.TheCatApi
import com.estiven.thecatapp.domain.model.BreedsInDTO
import com.estiven.thecatapp.domain.model.ImagesInDTO
import com.estiven.thecatapp.domain.repository.TheCatRepository
import javax.inject.Inject

class TheCatRepositoryImpl @Inject constructor(
    private val theCatApi: TheCatApi
) : TheCatRepository {

    override suspend fun getAllBreeds(
        page: Int,
        limit: Int
    ): Result<List<BreedsInDTO>> {
        return try {
            val result = theCatApi.getAllBreeds(page, limit)
            Result.success(result)
        } catch (exception: Exception) {
            Result.failure(exception)
        }
    }

    override suspend fun getImage(id: String): Result<ImagesInDTO> {
        return try {
            val result = theCatApi.getImage(id)
            Result.success(result)
        } catch (exception: Exception) {
            Result.failure(exception)
        }
    }
}