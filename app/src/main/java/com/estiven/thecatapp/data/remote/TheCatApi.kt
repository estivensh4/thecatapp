package com.estiven.thecatapp.data.remote

import com.estiven.thecatapp.data.util.EndPoints.BREEDS
import com.estiven.thecatapp.data.util.EndPoints.IMAGES
import com.estiven.thecatapp.domain.model.BreedsInDTO
import com.estiven.thecatapp.domain.model.ImagesInDTO
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TheCatApi {
    @GET(BREEDS)
    suspend fun getAllBreeds(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): List<BreedsInDTO>

    @GET(IMAGES)
    suspend fun getImage(
        @Path("id") id: String
    ): ImagesInDTO
}