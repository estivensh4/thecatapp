package com.estiven.thecatapp.data.di

import com.estiven.thecatapp.data.remote.TheCatApi
import com.estiven.thecatapp.data.repository.TheCatRepositoryImpl
import com.estiven.thecatapp.data.util.Environment.BASE_URL
import com.estiven.thecatapp.domain.repository.TheCatRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppDataModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }
            ).build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideTheCatApi(retrofit: Retrofit): TheCatApi = retrofit.create()

    @Provides
    @Singleton
    fun provideTheCatRepository(
        theCatApi: TheCatApi
    ): TheCatRepository = TheCatRepositoryImpl(theCatApi)
}