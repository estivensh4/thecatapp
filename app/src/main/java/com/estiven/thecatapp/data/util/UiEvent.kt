package com.estiven.thecatapp.data.util

import com.estiven.thecatapp.presentation.util.Screen

sealed class UiEvent {
    data class ShowSnackBar(val message: String) : UiEvent()
    data class Navigate(val screen: Screen) : UiEvent()
}
