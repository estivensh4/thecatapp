package com.estiven.thecatapp.shared.components

import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.imageLoader
import coil.request.CachePolicy
import coil.request.ImageRequest
import coil.transform.Transformation
import com.estiven.thecatapp.R

@Composable
fun CustomImageNetwork(
    modifier: Modifier = Modifier,
    url: String?,
    contentDescription: String,
    size: Dp = 64.dp,
    contentScale: ContentScale = ContentScale.Crop,
    transformations: Transformation? = null
) {
    val context = LocalContext.current
    val placeholderImage = R.drawable.loading
    val imageRequest = ImageRequest.Builder(context)
        .data(url ?: R.drawable.default_avatar)
        .memoryCacheKey(url)
        .diskCacheKey(url)
        .placeholder(placeholderImage)
        .error(placeholderImage)
        .diskCachePolicy(CachePolicy.DISABLED)
        .memoryCachePolicy(CachePolicy.DISABLED)
    if (transformations != null) {
        imageRequest.transformations(transformations)
    }
    AsyncImage(
        model = imageRequest.build(),
        modifier = modifier.size(size),
        contentDescription = contentDescription,
        imageLoader = context.imageLoader,
        contentScale = contentScale
    )
}