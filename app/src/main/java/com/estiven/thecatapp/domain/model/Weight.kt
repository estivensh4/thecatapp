package com.estiven.thecatapp.domain.model

data class Weight(
    val imperial: String = "",
    val metric: String = "",
)