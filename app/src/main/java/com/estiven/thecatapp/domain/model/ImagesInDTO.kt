package com.estiven.thecatapp.domain.model

data class ImagesInDTO(
    val url: String = ""
)
