package com.estiven.thecatapp.domain.repository

import com.estiven.thecatapp.domain.model.BreedsInDTO
import com.estiven.thecatapp.domain.model.ImagesInDTO

interface TheCatRepository {
    suspend fun getAllBreeds(
        page: Int,
        limit: Int
    ): Result<List<BreedsInDTO>>

    suspend fun getImage(id: String): Result<ImagesInDTO>
}