package com.estiven.thecatapp.domain.model

import com.google.gson.annotations.SerializedName

data class BreedsInDTO(
    val id: String = "",
    val name: String = "",
    val origin: String = "",
    val description: String = "",
    @SerializedName("life_span") val lifeSpan: String = "",
    @SerializedName("wikipedia_url") val wikipediaUrl: String = "",
    @SerializedName("reference_image_id") val referenceImageId: String = "",
    var image: String = "",
    val weight: Weight = Weight(),
    val temperament: String = ""
)
