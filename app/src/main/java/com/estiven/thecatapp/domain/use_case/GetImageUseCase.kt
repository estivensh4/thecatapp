package com.estiven.thecatapp.domain.use_case

import com.estiven.thecatapp.domain.repository.TheCatRepository

class GetImageUseCase(
    private val theCatRepository: TheCatRepository
) {
    suspend operator fun invoke(
        id: String
    ) = theCatRepository.getImage(id)
}
