package com.estiven.thecatapp.domain.use_case

data class UseCases(
    val getImageUseCase: GetImageUseCase
)