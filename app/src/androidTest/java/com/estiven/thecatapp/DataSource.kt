package com.estiven.thecatapp

import com.estiven.thecatapp.domain.model.BreedsInDTO
import com.estiven.thecatapp.domain.model.Weight

object DataSource {
    val breedList = listOf(
        BreedsInDTO(
            id = "chau",
            name = "Chausie",
            temperament = "Affectionate, Intelligent, Playful, Social",
            origin = "Egypt",
            lifeSpan = "12 - 14",
            description = "For those owners who desire a feline capable of evoking the great outdoors, the strikingly beautiful Chausie retains a bit of the wild in its appearance but has the house manners of our friendly, familiar moggies. Very playful, this cat needs a large amount of space to be able to fully embrace its hunting instincts.",
            wikipediaUrl = "https://en.wikipedia.org/wiki/Chausie",
            referenceImageId = "vJ3lEYgXr",
            image = "https://cdn2.thecatapi.com/images/0XYvRd7oD.jpg",
            weight = Weight(
                imperial = "8 - 15",
                metric = "4 - 7"
            )
        ),
        BreedsInDTO(
            id = "chau",
            name = "Chausie",
            temperament = "Affectionate, Intelligent, Playful, Social",
            origin = "Egypt",
            lifeSpan = "12 - 14",
            description = "For those owners who desire a feline capable of evoking the great outdoors, the strikingly beautiful Chausie retains a bit of the wild in its appearance but has the house manners of our friendly, familiar moggies. Very playful, this cat needs a large amount of space to be able to fully embrace its hunting instincts.",
            wikipediaUrl = "https://en.wikipedia.org/wiki/Chausie",
            referenceImageId = "vJ3lEYgXr",
            image = "https://cdn2.thecatapi.com/images/0XYvRd7oD.jpg",
            weight = Weight(
                imperial = "8 - 15",
                metric = "4 - 7"
            )
        ),
        BreedsInDTO(
            id = "csho",
            name = "Colorpoint Shorthair",
            temperament = "Affectionate, Intelligent, Playful, Social",
            origin = "Egypt",
            lifeSpan = "12 - 14",
            description = "Colorpoint Shorthairs are an affectionate breed, devoted and loyal to their people. Sensitive to their owner’s moods, Colorpoints are more than happy to sit at your side or on your lap and purr words of encouragement on a bad day. They will constantly seek out your lap whenever it is open and in the moments when your lap is preoccupied they will stretch out in sunny spots on the ground.",
            wikipediaUrl = "https://en.wikipedia.org/wiki/Chausie",
            referenceImageId = "oSpqGyUDS",
            image = "https://en.wikipedia.org/wiki/Colorpoint_Shorthair",
            weight = Weight(
                imperial = "8 - 15",
                metric = "4 - 7"
            )
        )
    )
}