package com.estiven.thecatapp

import com.estiven.thecatapp.domain.model.BreedsInDTO
import com.estiven.thecatapp.domain.model.ImagesInDTO
import com.estiven.thecatapp.domain.repository.TheCatRepository

class TheCatRepositoryFake : TheCatRepository {

    var shouldReturnError = false
    var searchResults = listOf<BreedsInDTO>()
    var imagesInDTO = ImagesInDTO()

    override suspend fun getAllBreeds(page: Int, limit: Int): Result<List<BreedsInDTO>> {
        return if (shouldReturnError) {
            Result.failure(Throwable())
        } else {
            Result.success(searchResults)
        }
    }

    override suspend fun getImage(id: String): Result<ImagesInDTO> {
        return if (shouldReturnError) {
            Result.failure(Throwable())
        } else {
            Result.success(imagesInDTO)
        }
    }
}
