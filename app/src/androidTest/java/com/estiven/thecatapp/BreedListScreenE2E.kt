package com.estiven.thecatapp

import androidx.compose.ui.res.stringResource
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.estiven.thecatapp.data.source.BreedsPagingSource
import com.estiven.thecatapp.domain.model.BreedsInDTO
import com.estiven.thecatapp.domain.use_case.GetImageUseCase
import com.estiven.thecatapp.domain.use_case.UseCases
import com.estiven.thecatapp.presentation.screen.detail.BreedDetailViewModel
import com.estiven.thecatapp.presentation.screen.detail.pages.BreedDetailScreen
import com.estiven.thecatapp.presentation.screen.list.BreedsViewModel
import com.estiven.thecatapp.presentation.screen.list.screen.BreedsListScreen
import com.estiven.thecatapp.presentation.theme.TheCatAppTheme
import com.estiven.thecatapp.presentation.util.Screen
import com.estiven.thecatapp.presentation.util.fromJson
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
class BreedListScreenE2E {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @get:Rule
    val composeRule = createComposeRule()

    private lateinit var theCatRepository: TheCatRepositoryFake
    private lateinit var useCases: UseCases
    private lateinit var breedsViewModel: BreedsViewModel
    private lateinit var breedDetailViewModel: BreedDetailViewModel
    private lateinit var navController: NavHostController
    private lateinit var testTagBreedList: String
    private lateinit var testTagItemBreedCard: String
    private lateinit var testTagMoreInfo: String

    @Before
    fun setUp() {
        theCatRepository = TheCatRepositoryFake()
        useCases = UseCases(
            getImageUseCase = GetImageUseCase(theCatRepository),
        )
        breedsViewModel = BreedsViewModel(
            useCases = useCases,
            theCatRepository = theCatRepository
        )
        breedDetailViewModel = BreedDetailViewModel()
        composeRule.setContent {
            TheCatAppTheme {
                navController = rememberNavController()
                testTagBreedList = stringResource(id = R.string.test_tag_breed_list)
                testTagItemBreedCard = stringResource(id = R.string.test_tag_item_breed)
                testTagMoreInfo = stringResource(id = R.string.text_button_more_info)
                NavHost(
                    navController = navController,
                    startDestination = Screen.BREEDS_LIST.route
                ) {
                    composable(Screen.BREEDS_LIST.route) {
                        BreedsListScreen(
                            navController = navController,
                            breedsViewModel = breedsViewModel
                        )
                    }
                    composable(
                        route = Screen.BREED_DETAIL.route + "?breed={breed}"
                    ) { navBackStackEntry ->
                        navBackStackEntry.arguments?.let { bundle ->
                            val breed = bundle.getString("breed")?.fromJson(BreedsInDTO::class.java)
                                ?: BreedsInDTO()
                            BreedDetailScreen(
                                breed = breed,
                                navController = navController,
                                breedDetailViewModel = breedDetailViewModel
                            )
                        }
                    }
                }
            }
        }
    }

    @Test
    fun scrollToFirstPosition_OnClickDetailItem_And_OnClickMoreInfo_ForDisplayedUrlExternalWikipedia(): Unit =
        runBlocking {
            theCatRepository.searchResults = DataSource.breedList
            breedsViewModel.resultPager = Pager(
                config = PagingConfig(
                    pageSize = 5,
                    initialLoadSize = 5,
                    prefetchDistance = 1
                ),
                pagingSourceFactory = {
                    BreedsPagingSource(theCatRepository)
                }
            ).flow
            assertThat(
                navController
                    .currentDestination
                    ?.route
                    ?.startsWith(Screen.BREEDS_LIST.route)
            ).isTrue()
            composeRule.onRoot().printToLog("TAG")
            composeRule.onNode(
                hasTestTag(testTagBreedList)
            ).assertIsDisplayed()
            composeRule.onNode(
                hasTestTag(testTagBreedList)
            ).performScrollToIndex(theCatRepository.searchResults.size - 1)
            composeRule
                .onAllNodesWithTag(testTagItemBreedCard)
                .onFirst()
                .assertIsDisplayed()
            composeRule
                .onAllNodesWithTag(testTagItemBreedCard)
                .onFirst()
                .performClick()
            assertThat(
                navController
                    .currentDestination
                    ?.route
                    ?.startsWith(Screen.BREED_DETAIL.route)
            ).isTrue()
            composeRule
                .onAllNodesWithText(testTagMoreInfo)
                .onFirst()
                .assertIsDisplayed()
            composeRule
                .onAllNodesWithText(testTagMoreInfo)
                .onFirst()
                .performClick()
        }
}