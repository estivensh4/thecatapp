package com.estiven.thecatapp.presentation.screen.list

import android.content.Context
import android.content.res.Configuration
import androidx.test.platform.app.InstrumentationRegistry
import com.estiven.thecatapp.TheCatRepositoryFake
import com.estiven.thecatapp.domain.use_case.GetImageUseCase
import com.estiven.thecatapp.domain.use_case.UseCases
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test


class BreedsViewModelTest {

    private lateinit var breedsViewModel: BreedsViewModel
    private lateinit var useCases: UseCases
    private lateinit var theCatRepositoryFake: TheCatRepositoryFake

    lateinit var instrumentationContext: Context

    @Before
    fun setUp() {
        instrumentationContext = InstrumentationRegistry.getInstrumentation().context
        theCatRepositoryFake = TheCatRepositoryFake()
        useCases = UseCases(
            getImageUseCase = GetImageUseCase(theCatRepositoryFake)
        )
        breedsViewModel = BreedsViewModel(
            useCases = useCases,
            theCatRepository = theCatRepositoryFake
        )
    }

    @Test
    fun validateReturnLandScapeIs4() {
        val configuration = instrumentationContext.resources.configuration
        configuration.orientation = Configuration.ORIENTATION_LANDSCAPE
        assertThat(breedsViewModel.validateLandScape(configuration) == 4).isTrue()
    }

    @Test
    fun validateReturnLandScapeIs2() {
        val configuration = instrumentationContext.resources.configuration
        configuration.orientation = Configuration.ORIENTATION_PORTRAIT
        assertThat(breedsViewModel.validateLandScape(configuration) == 2).isTrue()
    }
}