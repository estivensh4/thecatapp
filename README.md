# TheCatApi

La aplicación utiliza un conjunto de librerías Android Jetpack más Retrofit para enviar datos desde la API REST. La aplicación
utiliza Jetpack Compose.

[App iOS](https://gitlab.com/estivensh4/thecatappios)

## Arquitectura

El proyecto utiliza el patrón de arquitectura MVVM.

## Librerias

* [El estado y Jetpack Compose](https://developer.android.com/jetpack/compose/state?hl=es-419) - El
  estado de una app es cualquier valor que puede cambiar con el paso del tiempo. Esta es una
  definición muy amplia y abarca desde una base de datos de Room hasta una variable de una clase.
* [Material3](https://developer.android.com/jetpack/androidx/releases/compose-material3) - la siguiente
  evolución de Material Design. Material 3 incluye temas y componentes actualizados, y
  funciones de personalización de Material You, como el color dinámico.
* [Navigation Component](https://developer.android.com/jetpack/compose/navigation)
    - Puedes navegar entre los elementos que admiten composición y aprovechar la infraestructura y las funciones del componente Navigation
* [Dagger Hilt](https://developer.android.com/jetpack/compose/libraries?hl=es-419) - For Dependency
  Injection.
* [Retrofit](https://square.github.io/retrofit/) - Para acceder a la API Rest
* [Kotlin Flow](https://developer.android.com/kotlin/flow) - Para acceder a los datos de forma secuencial
* [Room](https://developer.android.com/jetpack/androidx/releases/room?hl=es-419) - La biblioteca de
  persistencias de Room brinda una capa de abstracción para SQLite que permite acceder a la base
  de datos sin problemas y, al mismo tiempo, aprovechar toda la potencia de SQLite.
* [Mock Web Server](https://github.com/square/okhttp/tree/master/mockwebserver) - Un servidor web con capacidad de scripting para probar clientes HTTP

## Capturas

| <img src="screenshots/dashboard.png" width=200/> | <img src="screenshots/detail.png" width=200/> |
|:------------------------------------------------:|:---------------------------------------------:|
