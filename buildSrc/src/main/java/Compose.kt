import org.gradle.api.artifacts.dsl.DependencyHandler

object Compose {
    const val version = "1.4.0"
    const val navigationVersion = "2.5.0"
    private const val ui = "androidx.compose.ui:ui"
    private const val uiToolingPreview = "androidx.compose.ui:ui-tooling-preview"
    private const val material3 = "androidx.compose.material3:material3"
    private const val hilt = "androidx.hilt:hilt-navigation-compose:1.0.0"
    private const val navigation = "androidx.navigation:navigation-compose:$navigationVersion"
    private const val iconsExtend = "androidx.compose.material:material-icons-extended"

    fun DependencyHandler.compose() {
        implementation(ui)
        implementation(uiToolingPreview)
        implementation(material3)
        implementation(hilt)
        implementation(navigation)
        implementation(iconsExtend)
    }
}