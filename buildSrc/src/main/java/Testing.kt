import org.gradle.api.artifacts.dsl.DependencyHandler

object Testing {
    const val testJunitVersion = "1.3.3"
    private const val coroutinesTestVersion = "1.5.1"
    private const val truthVersion = "1.1.3"
    private const val mockkVersion = "1.12.3"
    private const val mockWebServerVersion = "4.9.3"
    private const val testRunnerVersion = "1.4.0"

    private const val jUnit = "junit:junit:4.13.2"
    private const val extJunit = "androidx.test.ext:junit:1.1.5"
    private const val expresso = "androidx.test.espresso:espresso-core:3.5.1"
    private const val testJunit = "androidx.compose.ui:ui-test-junit4:$testJunitVersion"
    private const val uiTooling = "androidx.compose.ui:ui-tooling"
    private const val uiTestManifest = "androidx.compose.ui:ui-test-manifest"
    private const val coroutines =
        "org.jetbrains.kotlinx:kotlinx-coroutines-test:$coroutinesTestVersion"
    private const val truth = "com.google.truth:truth:$truthVersion"
    private const val mockk = "io.mockk:mockk:$mockkVersion"
    private const val mockkAndroid = "io.mockk:mockk-android:$mockkVersion"
    private const val mockWebServer = "com.squareup.okhttp3:mockwebserver:$mockWebServerVersion"
    private const val hiltTesting = "com.google.dagger:hilt-android-testing:${Dagger.version}"
    private const val testRunner = "androidx.test:runner:$testRunnerVersion"


    fun DependencyHandler.testing() {
        testImplementation(Testing.jUnit)
        testImplementation(Testing.extJunit)
        testImplementation(Testing.truth)
        testImplementation(Testing.coroutines)
        testImplementation(Testing.testJunit)
        testImplementation(Testing.mockk)
        testImplementation(Testing.mockWebServer)

        androidTestImplementation(extJunit)
        androidTestImplementation(expresso)
        androidTestImplementation(testJunit)
        androidTestImplementation(truth)
        androidTestImplementation(coroutines)
        androidTestImplementation(mockkAndroid)
        androidTestImplementation(mockWebServer)
        androidTestImplementation(hiltTesting)
        kaptAndroidTest(Dagger.compiler)
        androidTestImplementation(testRunner)
        debugImplementation(uiTooling)
        debugImplementation(uiTestManifest)
    }
}