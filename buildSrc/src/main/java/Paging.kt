import org.gradle.api.artifacts.dsl.DependencyHandler

object Paging {
    private const val version = "3.1.1"
    private const val runtime = "androidx.paging:paging-runtime:$version"
    private const val compose = "androidx.paging:paging-compose:1.0.0-alpha17"
    private const val commonKtx = "androidx.paging:paging-common-ktx:$version"

    fun DependencyHandler.paging3() {
        implementation(runtime)
        implementation(compose)
        implementation(commonKtx)
    }
}