import org.gradle.api.artifacts.dsl.DependencyHandler

object Coil {
    private const val coilVersion = "2.1.0"
    const val coilCompose = "io.coil-kt:coil-compose:$coilVersion"
    const val coilGif = "io.coil-kt:coil-gif:$coilVersion"

    fun DependencyHandler.coil() {
        implementation(coilCompose)
        implementation(coilGif)
    }
}