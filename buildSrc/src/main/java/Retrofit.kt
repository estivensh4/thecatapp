import org.gradle.api.artifacts.dsl.DependencyHandler

object Retrofit {
    private const val version = "2.9.0"
    private const val retrofit = "com.squareup.retrofit2:retrofit:$version"
    private const val converterGson = "com.squareup.retrofit2:converter-gson:$version"
    private const val okHttpBom = "com.squareup.okhttp3:okhttp-bom:4.10.0"
    private const val interceptor = "com.squareup.okhttp3:logging-interceptor"

    fun DependencyHandler.retrofit() {
        implementation(platform(okHttpBom))
        implementation(retrofit)
        implementation(converterGson)
        implementation(interceptor)
    }
}