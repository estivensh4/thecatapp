import org.gradle.api.artifacts.dsl.DependencyHandler

object Dagger {
    const val version = "2.44.2"
    private const val hilt = "com.google.dagger:hilt-android:$version"
    const val compiler = "com.google.dagger:hilt-android-compiler:$version"

    fun DependencyHandler.daggerHilt() {
        implementation(hilt)
        kapt(compiler)
    }
}